package org.eesoft.apptest.resources;

import com.codahale.metrics.annotation.Timed;
import org.eesoft.apptest.domain.Person;
import org.eesoft.apptest.messages.HelloMessage;
import org.eesoft.apptest.messages.HelloResponder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * Created with IntelliJ IDEA.
 * User: matt
 * Date: 7/14/13
 * Time: 10:30 AM
 * To change this template use File | Settings | File Templates.
 */
@Singleton
@Path("/hello")
@Produces(MediaType.APPLICATION_JSON)
public class HelloResource {

    private static final Logger log = LoggerFactory.getLogger(HelloResource.class);

    private final HelloResponder responder;

    @Inject
    public HelloResource(final HelloResponder responder) {
        this.responder = responder;
    }

    @GET
    @Timed(name = "helloTimed")
    public HelloMessage hello(@QueryParam("name") final String name) {
        final Person person = new Person(name);

        log.info("Greeting {}", name);

        return responder.sayHello(person);
    }
}
