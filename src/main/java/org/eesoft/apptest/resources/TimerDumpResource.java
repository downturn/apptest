package org.eesoft.apptest.resources;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Snapshot;
import com.codahale.metrics.Timer;
import static com.codahale.metrics.MetricRegistry.name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: matt
 * Date: 7/14/13
 * Time: 6:58 PM
 * To change this template use File | Settings | File Templates.
 */
@Singleton
@Path("/dump")
public class TimerDumpResource {

    private static final Logger log = LoggerFactory.getLogger(TimerDumpResource.class);

    private final MetricRegistry metricRegistry;

    @Inject
    public TimerDumpResource(final MetricRegistry metricRegistry) {
        this.metricRegistry = metricRegistry;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Object> dumpTimer() {
        final Timer helloTimer = metricRegistry.timer(name(HelloResource.class, "helloTimed"));
        final Snapshot snapshot = helloTimer.getSnapshot();
        final Map<String, Object> dump = new HashMap<>();

        dump.put("meanRate", helloTimer.getMeanRate());
        dump.put("oneMinuteRate", helloTimer.getOneMinuteRate());
        dump.put("max", snapshot.getMax());
        dump.put("min", snapshot.getMin());
        dump.put("median", snapshot.getMedian());
        dump.put("snapshotValues", Arrays.asList(snapshot.getValues()));

        return dump;
    }
}
