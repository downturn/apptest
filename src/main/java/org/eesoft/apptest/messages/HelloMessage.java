package org.eesoft.apptest.messages;

/**
 * Created with IntelliJ IDEA.
 * User: matt
 * Date: 7/14/13
 * Time: 2:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class HelloMessage {

    private String message;

    public HelloMessage(final String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
