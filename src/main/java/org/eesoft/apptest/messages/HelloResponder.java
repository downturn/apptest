package org.eesoft.apptest.messages;

import org.eesoft.apptest.domain.Person;

/**
 * Created with IntelliJ IDEA.
 * User: matt
 * Date: 7/14/13
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
public interface HelloResponder {
    HelloMessage sayHello(Person person);
}
