package org.eesoft.apptest.messages;

import org.eesoft.apptest.domain.Person;

/**
 * Created with IntelliJ IDEA.
 * User: matt
 * Date: 7/14/13
 * Time: 2:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class InformalHelloResponder implements HelloResponder {

    @Override
    public HelloMessage sayHello(final Person person) {
        final String name = person.getName();

        if (name == null) {
            return new HelloMessage("Hey, dude");
        } else {
            return new HelloMessage(String.format("Hey, %s", name));
        }
    }
}
