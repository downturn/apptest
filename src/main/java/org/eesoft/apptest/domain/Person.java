package org.eesoft.apptest.domain;

/**
 * Created with IntelliJ IDEA.
 * User: matt
 * Date: 7/14/13
 * Time: 2:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class Person {

    private String name;

    public Person() {

    }

    public Person(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

}
