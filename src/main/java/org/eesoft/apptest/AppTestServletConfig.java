package org.eesoft.apptest;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.jersey.InstrumentedResourceMethodDispatchAdapter;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import org.eesoft.apptest.messages.HelloResponder;
import org.eesoft.apptest.messages.InformalHelloResponder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: matt
 * Date: 7/14/13
 * Time: 1:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class AppTestServletConfig extends GuiceServletContextListener {

    @Override
    protected Injector getInjector() {
        return Guice.createInjector(new JerseyServletModule() {

            @Override
            protected void configureServlets() {
                final MetricRegistry metricRegistry = new MetricRegistry();
                final InstrumentedResourceMethodDispatchAdapter adapter =
                        new InstrumentedResourceMethodDispatchAdapter(metricRegistry);

                bind(MetricRegistry.class).toInstance(metricRegistry);
                bind(InstrumentedResourceMethodDispatchAdapter.class).toInstance(adapter);

                bind(HelloResponder.class).to(InformalHelloResponder.class);

                final Map<String, String> settings = new HashMap<>();

                settings.put("com.sun.jersey.config.property.packages", "org.eesoft.apptest.resources");

                serve("/*").with(GuiceContainer.class, settings);
            }
        });
    }
}
