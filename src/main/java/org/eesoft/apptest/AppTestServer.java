package org.eesoft.apptest;

import com.google.inject.servlet.GuiceFilter;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with IntelliJ IDEA.
 * User: matt
 * Date: 7/13/13
 * Time: 11:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class AppTestServer {

    private static final Logger log = LoggerFactory.getLogger(AppTestServer.class);

    public static void main(final String[] argv) throws Exception {
        log.info("Starting up...");
        final Server server = new Server(8080);
        final ServletContextHandler contextHandler = new ServletContextHandler(server, "/");

        contextHandler.addEventListener(new AppTestServletConfig());
        contextHandler.addFilter(GuiceFilter.class, "/*", null);
        contextHandler.addServlet(DefaultServlet.class, "/");

        server.start();
        log.info("Started");
        server.join();
    }
}
